'use strict';
const btnNewGame = document.querySelector('.btn--new');
const btnRollDice = document.querySelector('.btn--roll');
const btnHold = document.querySelector('.btn--hold');
const dice = document.querySelector('.dice');
dice.style.display = 'none';
let currentPlayer = 0;

const WIN = 100;

const rollDice = () => {
    dice.style.display = 'block';
    const diceResult = Math.trunc(Math.random() * 6)+1;
    const diceThrowImg =  `dice-${diceResult}.png`;
    dice.setAttribute('src', diceThrowImg);
    return diceResult;
};

const setCounter = (playerId, diceResult) => {
    const playerCounter = document.querySelector(`#current--${playerId}`);
    if (diceResult === 1){
        playerCounter.textContent = `0`;
        switchPlayer();
    } else {
        playerCounter.textContent = `${getPlayerCurrent(playerId) + diceResult}`;
    }
};

const resetCounter = (playerId) => {
    const playerCounter = document.querySelector(`#current--${playerId}`);
    playerCounter.textContent = `0`;
};

const getPlayerCurrent = (playerId) => {
    const playerCounter = document.querySelector(`#current--${playerId}`);
    return Number(playerCounter.textContent);
};

const getPlayerScore = (playerId) => {
    const playerScore = document.querySelector(`#score--${playerId}`);
    return Number(playerScore.textContent);
};

const setScore = (playerId, score) =>{
    const playerScore = document.querySelector(`#score--${playerId}`);
    playerScore.textContent = `${getPlayerScore(playerId) + score}`;
};

const resetScore = (playerId) =>{
    const playerScore = document.querySelector(`#score--${playerId}`);
    playerScore.textContent = `0`;
};

resetScore(0);
resetScore(1);

const switchPlayer = () => {
    setScore(currentPlayer, getPlayerCurrent(currentPlayer));
    resetCounter(currentPlayer);

    if (getPlayerScore(currentPlayer) >= WIN){
        alert(`Player ${currentPlayer+1} wins.`);
    } else {
        document.querySelector(`.player--${currentPlayer}`).classList.remove('player--active');
        if (currentPlayer === 0) {
            currentPlayer++;
        }else if (currentPlayer === 1) {
            currentPlayer--;
        }

        document.querySelector(`.player--${currentPlayer}`).classList.add('player--active');
    }
};

const rollDiceProcess = () => {
    if (getPlayerScore(currentPlayer) >= WIN){
        alert(`Player ${currentPlayer+1} already won. Click on new game.`);
    } else {
        const diceResult = rollDice();
        setCounter(currentPlayer, diceResult);
    }
};

const _new = () => {
    dice.style.display = 'none';
    resetScore(0);
    resetScore(1);
    resetCounter(0);
    resetCounter(1);
    currentPlayer = 0;
    document.querySelector(`.player--${currentPlayer}`).classList.add('player--active');
    document.querySelector(`.player--${currentPlayer+1}`).classList.remove('player--active');
};

btnRollDice.addEventListener('click', rollDiceProcess);
btnNewGame.addEventListener('click', _new);
btnHold.addEventListener('click', switchPlayer);
